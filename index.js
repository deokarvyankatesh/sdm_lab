const mysql = require('mysql2');
const express = require('express');
const app = express();

app.use(express.json);

const connection  = mysql.createConnection({
    host: '172.17.0.2',
    user: 'root',
    password: 'root',
    database: 'punedb'
})
connection.connect();

app.get("/",(req,res)=>{
    let query = "select * from movie";
    connection.query(query,(error, result)=>{
        if(error != null){
            res.send("Some Internal Error");
            console.log(error);
            res.end();
        }else{
            res.send(JSON.stringify(result));
            res.end();
        }
    })
})

app.post("/:n", (req, res)=>{
    let query = `insert into movie values(${req.params.n}, '${req.body.title}', '${req.body.release_date}', '${req.body.release_time}', '${req.body.director_name}')`;
    connection.query(query, (error, result)=>{
        if(error != null){
            res.send("Some Internal Error");
            console.log(error);
            res.end();
        }else{
            res.send(result);
            res.end();
        }
    })
})

app.delete("/:n", (req,res)=>{
    let query = `delete from movie where id = ${req.params.n}`;
    connection.query(query,(error, result)=>{
        if(error != null){
            res.send("Some Internal Error");
            console.log(error);
            res.end();
        }else{
            res.send(result);
            res.end();
        }
    })
})

connection.end();
app.listen(9090, ()=>{
    console.log("Server started and listening on 9090")
})